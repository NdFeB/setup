#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

"""
Global documentation (TODO: convert it to Ansible format)

Find the latest version tag.
Version syntax must comply with PEP440 (compatible with SemVer) (see packaging.version).
If repository contains non PEP440 tags, and no filters are given, they will be completely ignored.
This can be worked around using filters.

Parameters
----------
repository: str
    Git repository to parse tags from

version_constraint: str, optional
    Search for the latest version satisfying the given constraint. Must be a valid PEP440 syntax.
    Example: version_constraint='1.24', will return 1.24.17, even if 1.25.0 exists.

refs: list of str, optional
    Fetch only tags satisfying one of these tags refs.
    Each ref must comply with git ls-remote refs syntax.

filters: list of str, optional
    List of patterns that should be removed from tag names before trying to interpret them as Version.
    Each filter must be a valid `re` regex.
    Example: ['^me-', '-testing$'] will interpret tag 'me-1.2.3-testing' as '1.2.3'. Without filters, this tag would be ignored.

allow_prerelease: bool, default=False
    Allow to return a prerelease or a dev tag (alpha, beta, dev, rc, etc.).

Notes
-----
By default, non-PEP440 tags are ignored. With `filters`, they can be included in the search.
However, if filters are given, any non-PEP440 filtered tag will make this function to raise an InvalidVersion exception.

See Also
--------
packaging.version:
    Python module implementing PEP440 versioning features.

git-ls-remote(1)
"""

DOCUMENTATION = r'''
---
module: my_test

short_description: This is my test module

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

import re
import subprocess
from ansible.module_utils.basic import AnsibleModule
from packaging.version import Version, InvalidVersion

# Test cases:
# - 1.14.1
# - 1.0.1
# - 1.24
# - 1
# - 1a

"""
import re
from packaging.version import Version
a=['v1.1.1','1.10.1','1.24','1.23','toto-1.17', '1.22-tutu']
p = ['^toto-', '-tutu$']
m = re.compile('|'.join(p))
sorted(a)
sorted(a, key=lambda v: Version(re.sub(m,'',v)))
"""

# NOTE: after applying filters, we may have duplicate tag filtered names.
# Tag names are unique in a repo, but imagine the following situation:
# tag: v1.25.2
# tag: myfork-v1.25.2
# And you set filter '^myfork-'

# Now, 2 tags have the same filtered name: v1.25.2
# To avoid this situation, add a 'refs' equal to 'myfork-*', so only tags
# starting with 'myfork-' will be fetched.


def is_valid_version(version: str) -> bool:
    """
    Return True if given `version` is PEP440 compliant, else False.
    """
    try:
        Version(version)
        return True
    except InvalidVersion:
        return False


def fetch_tags(repo: str, refs: list[str], post_fetch_filters: list[str] = []) -> list[dict]:
    """
    Parameters
    ----------
    repository: str
        Git repository to fetch tags from

    refs: list of str, optional
        Fetch only tags satisfying one of these tags refs.
        Each ref must comply with git ls-remote refs syntax.

    post_fetch_filters: list of str, optional
        List of patterns that should be removed from tag names (saved as `filtered_name`).
        Each filter must be a valid `re` regex.
        Example: for a tag name `me-1.2.3-testing`, `['^me-', '-testing$']` will set `filtered_name` to `1.2.3`.
        Without filters, this tag would be pruned by `prune_invalid_version`.

    Returns
    -------
    list of dict

    Notes
    -----
    After applying filters, there may be duplicate tag filtered names.
    Tag names are unique in a repo. Consider the following 2 tags:
    - v1.25.2
    - myfork-v1.25.2
    And you set `post_fetch_filters` to `['^myfork-']`
    Now, 2 tags have the same filtered name: `v1.25.2`
    If you want to avoid this situation, add a `refs` equal to `myfork-*`, so only tags
    starting with `myfork-` are fetched.

    See Also
    --------
    git-ls-remote(1)
    """
    cmd = 'git ls-remote --refs --tags'.split() + [repo] + refs
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result.check_returncode()
    lines = result.stdout.splitlines()

    filters = re.compile('|'.join(post_fetch_filters))
    return [
        {
            'commit': commit,
            'name': name.replace('refs/tags/', '', 1),
            'filtered_name':
                re.sub(filters, '', name.replace('refs/tags/', '', 1))
                if post_fetch_filters
                else name.replace('refs/tags/', '', 1)
        }
        for line in lines
        for commit, name in (line.decode('utf-8').split(),)
    ]


def prune_invalid_version(tags: list[dict]):
    """
    Returns a subset of `tags`, pruning any entry `e` where `e['filtered_name']` is not PEP440 compliant.
    """
    return [tag for tag in tags if is_valid_version(tag['filtered_name'])]


def find_latest_version_from_tags(
        tags: list[dict],
        version_constraint: str = '',
        allow_prerelease: bool = False,
        ) -> dict | None:
    """
    Find the latest version tag.
    Version syntax must comply with PEP440 (compatible with SemVer) (see packaging.version).
    If repository contains non PEP440 tags, and no filters are given, they will be completely ignored.
    This can be worked around using filters.

    Parameters
    ----------
    version_constraint: str, optional
        Search for the latest version satisfying the given `constraint`. Must be a valid PEP440 syntax.
        Example: `version_constraint='1.24'` will return `1.24.17`, even if `1.25.0` exists.

    allow_prerelease: bool, default=False
        Allow to return a prerelease or a dev tag (alpha, beta, dev, rc, etc.).

    Notes
    -----
    By default, non-PEP440 tags are ignored. With `filters`, they can be included in the search.
    However, if filters are given, any non-PEP440 filtered tag will make this function to raise an InvalidVersion exception.

    See Also
    --------
    packaging.version:
        Python module implementing PEP440 versioning features.
    """
    if version_constraint:
        req_vers = Version(version_constraint)
        if allow_prerelease and not req_vers.is_prerelease and not req_vers.is_devrelease:
            req_vers = Version(version_constraint + 'a')

    for tag in tags:
        try:
            tag_name = tag['filtered_name']

            # If current tag is lower than requested constraint, return now
            if version_constraint:
                tag_vers = Version(tag_name)
                if tag_vers < req_vers:
                    return None

            # Tags are version-sorted from latest to oldest. First match is the latest available
            if not version_constraint or all(tag_vers.release[i] == req_vers.release[i] for i in range(len(req_vers.release))):
                # Return tag if not a prerelease, or if we allow prereleases
                if allow_prerelease or (not tag_vers.is_prerelease and not tag_vers.is_devrelease):
                    return tag

        except InvalidVersion:
            # Ingore non PEP440 tags
            pass
    return None


def run_module():
    module_args = dict(
        repository=dict(type='str', required=True, aliases=['repo']),
        version_constraint=dict(type='str', required=False, default='', aliases=['version', 'constraint']),
        refs=dict(type='list', elements='str', required=False, default=[]),
        filters=dict(type='list', elements='str', required=False, default=[]),
        allow_prerelease=dict(type='bool', required=False, default=False),
        fail_when_not_found=dict(type='bool', required=False, default=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    result = dict(
        changed=False,
        tag_name='',
        commit_id='',
        version=dict(
            type='',
            major='',
            minor='',
            patch='',
        )
    )

    try:
        # This module only works on PEP404 Versions, so prune any not compliant tag
        # prune_invalid_version filters tags based on their filtered_name
        tags = prune_invalid_version(
            fetch_tags(
                module.params['repository'],
                module.params['refs'],
                module.params['filters'],
            )
        )
        tags.sort(reverse=True, key=lambda x: Version(x['filtered_name']))

        tag = find_latest_version_from_tags(
            tags,
            module.params['version_constraint'],
            module.params['allow_prerelease'],
        )
    except subprocess.CalledProcessError as e:
        # No need to include stdout/stderr, ansible already does it
        module.fail_json(
            msg=f'Error during git execution',
            cmd=e.cmd,
            return_code=e.returncode,
            stdout_lines=[l for l in e.stdout.splitlines() if l],
            stderr_lines=[l for l in e.stderr.splitlines() if l],
        )

    if not tag:
        if module.params['fail_when_not_found']:
            msg = f'No tag found in repository \'{module.params["repository"]}\''
            if module.params['version_constraint']:
                msg += f' for version \'{module.params["version_constraint"]}\''
            if module.params['refs']:
                msg += f' with refs {module.params["refs"]}'
            if module.params['filters']:
                msg += f' after applying filters {module.params["filters"]}'
            if module.params['allow_prerelease']:
                msg += f' (including prereleases)'
            else:
                msg += f' (ignoring prereleases)'

            module.fail_json(msg=msg)
        else:
            module.exit_json(changed=False)
    else:
        result['tag_name'], result['commit_id'] = tag['name'], tag['commit']


    v = Version(result['tag_name'])
    if v.is_prerelease:
        result['version']['type'] = 'prerelease'
    elif v.is_devrelease:
        result['version']['type'] = 'devrelease'
    elif v.is_postrelease:
        result['version']['type'] = 'postrelease'
    else:
        result['version']['type'] = 'release'

    result['version']['major'] = v.major
    result['version']['minor'] = v.minor
    result['version']['patch'] = v.micro

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
