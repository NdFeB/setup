#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: my_test

short_description: This is my test module

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

import json
from ansible.module_utils.basic import AnsibleModule
from packaging.version import Version, InvalidVersion
from urllib.request import urlopen

# Test cases:
# - 1.14.1
# - 1.0.1
# - 1.24
# - 1
# - 1a

def find_latest_tag_by_prefix(repo: str, constraint: str = '', allow_prerelease: bool = False) -> dict:
    page=1
    per_page=100
    if not constraint:
        # TODO : EXCLUDE PRERELEASE
        with urlopen(f'https://api.github.com/repos/{repo}/tags?per_page=1&page=1') as resp:
            res = json.loads(resp.read().decode('utf-8'))
            if len(res) > 0: return res[0]
            else: return {}

    constraint_vers = Version(constraint)
    # If allow_prerelease is True but given prefix is not a prerelease, make it a prerelease
    # so we don't give up too early when comparing tag_vers < constraint_vers (alpha are always lower)
    if allow_prerelease and not constraint_vers.is_prerelease and not constraint_vers.is_devrelease:
        constraint_vers = Version(constraint + 'a')

    # GitHub returns tags ordered from the highest, semver wise. The first matching prefix we find is the latest available
    while True:
        with urlopen(f'https://api.github.com/repos/{repo}/tags?per_page={per_page}&page={page}') as resp:
            tags = json.loads(resp.read().decode('utf-8'))

        for tag in tags:
            if tag.get('name'):
                try:
                    tag_vers = Version(tag.get('name'))
                    # If current tag is lower than requested prefix, return now
                    if tag_vers < constraint_vers:
                        return {}

                    if all(tag_vers.release[i] == constraint_vers.release[i] for i in range(len(constraint_vers.release))):
                        # Return tag if not a prerelease, or if we allow prereleases
                        if allow_prerelease or (not tag_vers.is_prerelease and not tag_vers.is_devrelease):
                            return tag

                except InvalidVersion:
                    # Ignore non semantic tags
                    pass

        if 'rel="next"' in resp.getheader('link', ''): page +=1
        else: return {}


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        version=dict(type='str', required=True),
        repository=dict(type='str', required=True, aliases=['repo']),
        allow_prerelease=dict(type='bool', required=False, default=False),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    result = dict(
        changed=False,
        version_type="",
        # GitHub repos tags API format
        tag=dict(
            name="",
            zipball_url="",
            tarball_url="",
            commit=dict(
                sha="",
                url="",
            ),
            node_id="",
        ),
    )

    tag = find_latest_tag_by_prefix(
        module.params['repository'],
        module.params['version'],
        module.params["allow_prerelease"],
    )
    if not tag:
        module.fail_json(msg=f'No tag found for version \'{module.params["version"]}\'')
    else:
        result['tag'] = tag

    v = Version(tag.get('name'))
    if v.is_prerelease:
        result['version_type'] = 'prerelease'
    elif v.is_devrelease:
        result['version_type'] = 'devrelease'
    elif v.is_postrelease:
        result['version_type'] = 'postrelease'
    else:
        result['version_type'] = 'release'

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()
