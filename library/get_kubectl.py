#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: my_test

short_description: This is my test module

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

import json
from ansible.module_utils.basic import AnsibleModule
from packaging.version import Version, InvalidVersion
from urllib.request import Request, urlopen
from urllib.error import HTTPError

# Test cases:
# - 1.14.1 (no sha256)
# - 1.0.1
# - 1.24
# - 1
# - 1a

#def download_package_manifest(version: str) -> list:
#    packages = []
#    fields = [
#        'package',
#        'version',
#        'architecture',
#        'filename',
#        'md5sum',
#        'sha1',
#        'sha256',
#    ]
#    with urlopen(f'https://pkgs.k8s.io/core:/stable:/v{version}/deb/Packages') as resp:
#        packages_raw = resp.read().decode('utf-8').split('\n\n')
#
#    for p in packages_raw:
#        package={}
#        for line in p.split('\n'):
#            field = line.split(': ')[0].lower()
#            if field in fields:
#                package[field] = ''.join(line.split(': ')[1:])
#        if package.get('package') == 'kubectl':
#            packages.append(package)
#
#    return packages

def find_latest_tag_by_prefix(prefix: str, allow_prerelease: bool = False) -> dict:
    page=1
    per_page=100
    prefix_vers = Version(prefix)
    prefix_vers_len = len(prefix_vers.release)
    # If allow_prerelease is True but given prefix is not a prerelease, make it a prerelease
    # so we don't give up too early when comparing tag_vers < prefix_vers
    if allow_prerelease and not prefix_vers.is_prerelease and not prefix_vers.is_devrelease:
        prefix_vers = Version(prefix + 'a')
    # GitHub returns tags ordered from the highest, semver wise. The first matching prefix we find is the latest available
    while True:
        with urlopen(f'https://api.github.com/repos/kubernetes/kubernetes/tags?per_page={per_page}&page={page}') as resp:
            tags = json.loads(resp.read().decode('utf-8'))

        for tag in tags:
            if tag.get('name'):
                try:
                    tag_vers = Version(tag.get('name'))
                    # If current tag is lower than requested prefix, return now
                    if tag_vers < prefix_vers:
                        return {}

                    # Validate tag: Version.release = (major, minor, micro)
                    # If requested prefix is 1.28, then check that at least major == 1 and minor == 28
                    if all(tag_vers.release[i] == prefix_vers.release[i] for i in range(prefix_vers_len)):
                        # Return tag if not a prerelease, or if we allow prereleases
                        if allow_prerelease or (not tag_vers.is_prerelease and not tag_vers.is_devrelease):
                            return tag

                except InvalidVersion:
                    # If kubernetes team releases a non semantic tag, just ignore it
                    pass

        if 'rel="next"' in resp.getheader('link', ''): page +=1
        else: return {}


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        version=dict(type='str', required=True),
        architecture=dict(type='str', required=False, default='amd64', aliases=['arch']),
        platform=dict(type='str', required=False, default='linux'),
        fail_if_not_found=dict(type='bool', required=False, default=True),
        allow_prerelease=dict(type='bool', required=False, default=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    result = dict(
        changed=False,
        requested_version=module.params['version'],
        architecture=module.params['architecture'],
        platform=module.params['platform'],
        version=None,
        bin_url=None,
        bin_sha256=None,
        type=None,
    )

    tmp_vers = module.params['version']
    if not tmp_vers.startswith('v'):
        tmp_vers = f'v{tmp_vers}'

    try:
        # Check if provided tag is already a valid tag (not a prefix)
        bin_url = f'https://dl.k8s.io/release/{tmp_vers}/bin/{module.params["platform"]}/{module.params["architecture"]}/kubectl'
        if urlopen(Request(bin_url, method='HEAD')).status == 200:
            version = tmp_vers

    except HTTPError as e:
        if e.status != 404:
            raise e
        version = find_latest_tag_by_prefix(tmp_vers, module.params["allow_prerelease"]).get('name')
        if not version:
            if module.params['fail_if_not_found']:
                module.fail_json(msg=f'No tag found for version \'{module.params["version"]}\'')
            else:
                module.exit_json(**result)
        else:
            bin_url = f'https://dl.k8s.io/release/{version}/bin/{module.params["platform"]}/{module.params["architecture"]}/kubectl'

    if Version(version).is_prerelease:
        release_type = 'prerelease'
    elif Version(version).is_devrelease:
        release_type = 'devrelease'
    elif Version(version).is_postrelease:
        release_type = 'postrelease'
    else:
        release_type = 'release'

    result['version'] = version
    result['type'] = release_type
    result['bin_url'] = bin_url
    try:
        result['bin_sha256'] = urlopen(f'{bin_url}.sha256').read().decode('utf-8').strip()
    except HTTPError as e:
        if e.status == 404:
            # Older releases have sha1 but not sha256
            result.pop('bin_sha256')
            result['bin_sha1'] = urlopen(f'{bin_url}.sha1').read().decode('utf-8').strip()
        else:
            raise e

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
