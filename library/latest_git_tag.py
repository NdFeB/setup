#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: my_test

short_description: This is my test module

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

import subprocess
from ansible.module_utils.basic import AnsibleModule
from packaging.version import Version, InvalidVersion

# Test cases:
# - 1.14.1
# - 1.0.1
# - 1.24
# - 1
# - 1a

# github/torvalds/linux -> constraint 6.6 without prerelease fails, but v6.6 exists
# git ls-remote --sort -v:refname places rc before final releases...
# fixed with custom sorting. non PEP440 version are pruned (they used to be ignored anyway).


def fetch_tags(repo: str, refs: list[str]) -> list[dict]:
    cmd = 'git ls-remote --refs --tags'.split() + [repo] + refs
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result.check_returncode()
    lines = result.stdout.splitlines()
    return [
        {'commit': commit, 'name': name.replace('refs/tags/', '', 1)}
        for line in lines
        for commit, name in (line.decode('utf-8').split(),)
    ]


def is_valid_version(version: str) -> bool:
    try:
        Version(version)
        return True
    except InvalidVersion:
        return False


def prune_invalid_version(tags: list[dict]):
    return [tag for tag in tags if is_valid_version(tag['name'])]


def find_latest_version_from_tags(
        repository: str,
        patterns: list[str] = [],
        version_constraint: str = '',
        allow_prerelease: bool = False,
        ) -> dict | None:

    # Do not use `--sort -v:refname` from git: it sorts rc tags before final releases
    #cmd = 'git ls-remote --ref --tags --sort -v:refname'.split() + [repository] + patterns
    # cmd = 'git ls-remote --ref --tags'.split() + [repository] + patterns
    # result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # result.check_returncode()
    # tags = result.stdout.splitlines()
    tags = prune_invalid_version(fetch_tags(repository, patterns))

    tags.sort(reverse=True, key=lambda tag: Version(tag['name']))

    if version_constraint:
        req_vers = Version(version_constraint)
        if allow_prerelease and not req_vers.is_prerelease and not req_vers.is_devrelease:
            req_vers = Version(version_constraint + 'a')


    for tag in tags:
        # try:
            #commit, ref = tag.split()
            #tag_name = ref.decode('utf-8').replace('refs/tags/', '', 1)
            commit, tag_name = tag['commit'], tag['name']
            tag_vers = Version(tag_name)

            # If current tag is lower than requested constraint, return now
            if version_constraint and tag_vers < req_vers:
                return None

            # Tags are version-sorted from latest to oldest. First match is the latest available
            if not version_constraint or all(tag_vers.release[i] == req_vers.release[i] for i in range(len(req_vers.release))):
                # Return tag if not a prerelease, or if we allow prereleases
                if allow_prerelease or (not tag_vers.is_prerelease and not tag_vers.is_devrelease):
                    #return {'name': tag_name, 'commit': commit.decode('utf-8')}
                    return tag

        # Ignore non semantic tags
        # except InvalidVersion:
        #     pass
    return None


def run_module():
    module_args = dict(
        repository=dict(type='str', required=True, aliases=['repo']),
        patterns=dict(type='list', elements='str', required=False, default=[]),
        version_constraint=dict(type='str', required=False, default='', aliases=['version', 'constraint']),
        allow_prerelease=dict(type='bool', required=False, default=False),
        fail_when_not_found=dict(type='bool', required=False, default=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True,
    )

    result = dict(
        changed=False,
        tag_name='',
        commit_id='',
        version=dict(
            type='',
            major='',
            minor='',
            patch='',
        )
    )

    try:
        tag = find_latest_version_from_tags(
            module.params['repository'],
            module.params['patterns'],
            module.params['version_constraint'],
            module.params["allow_prerelease"],
        )
    except subprocess.CalledProcessError as e:
        # No need to include stdout/stderr, ansible already does it
        module.fail_json(
            msg=f'Error during git execution',
            cmd=e.cmd,
            return_code=e.returncode,
            stdout_lines=[l for l in e.stdout.splitlines() if l],
            stderr_lines=[l for l in e.stderr.splitlines() if l],
        )

    if not tag:
        if module.params['fail_when_not_found']:
            msg = f'No tag found in repository \'{module.params["repository"]}\''
            if module.params['version_constraint']:
                msg += f' for version \'{module.params["version_constraint"]}\''
            if module.params['patterns']:
                msg += f' with patterns {module.params["patterns"]}'

            module.fail_json(msg=msg)
        else:
            module.exit_json(changed=False)
    else:
        result['tag_name'], result['commit_id'] = tag['name'], tag['commit']


    v = Version(result['tag_name'])
    if v.is_prerelease:
        result['version']['type'] = 'prerelease'
    elif v.is_devrelease:
        result['version']['type'] = 'devrelease'
    elif v.is_postrelease:
        result['version']['type'] = 'postrelease'
    else:
        result['version']['type'] = 'release'

    result['version']['major'] = v.major
    result['version']['minor'] = v.minor
    result['version']['patch'] = v.micro

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
