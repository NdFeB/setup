# setup

Setup playbook for personal env.

## Prequisites

- A user (for full features, with sudoer privileges)
- python3
- ansible

Setup ansible in venv:

```sh
python3 -m venv ansible-venv
source ansible-venv/bin/activate
python -m pip install -U pip

# ansible-core is enough, we only use builtin modules
python -m pip install ansible-core
```
